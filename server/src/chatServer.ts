import { createServer, Server } from 'http';
import * as socketIo from 'socket.io';
import { config } from 'dotenv';
import { resolve } from 'path';

export class ChatServer {
    private server: Server;
    private io: SocketIO.Server;

    constructor() {
        this.init();
        this.handleEvents();
    }

    init() {
        // Load environment variables
        config({
            path: resolve(__dirname, '../.env')
        });

        this.server = createServer();
        this.io = socketIo(this.server);

        this.server.listen(process.env.PORT, () => {
            console.log(`Server running on port ${process.env.PORT}`);
        });
    }

    handleEvents() {
        this.io.on('connection', (socket) => {
            console.log(`${socket.handshake.address} connected`);

            socket.emit('connectedUsers', Object.keys(this.io.sockets.sockets).length);

            socket.on('message', (text) => {
                this.io.sockets.send({
                    text,
                    dateTime: new Date()
                });
            });
        });
    }
}
