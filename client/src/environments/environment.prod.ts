export const environment = {
    production: true,
    chatServerUrl: 'http://localhost:5555',
    echoServerUrl: 'http://localhost:6001'
};
