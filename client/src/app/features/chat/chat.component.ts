import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatService } from '../../services/chat.service';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {
    public message: string;
    public messages: object[];

    constructor(private chat: ChatService) {
        this.messages = [];

        chat.messages.subscribe(
            next => {
                this.messages.push(next);
            }
        );
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.chat.messages.unsubscribe();
    }

    public sendMessage() {
        this.chat.sendMessage(this.message);
        this.message = '';
    }

}
