import { Injectable } from '@angular/core';

import Echo from 'laravel-echo';
import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ChatService {
    private socket: any;
    private echo: Echo;

    public messages: Subject<object>;

    constructor() {
        this.messages = new Subject<object>();
        this.init();
        this.listenToEvents();
    }

    private init() {
        // Make a connection with the chat server
        this.socket = io(environment.chatServerUrl);
        this.socket.on('connect', () => {
            this.messages.next({
                text: `Welcome!`
            });

            this.socket.on('connectedUsers', (data: number) => {
                this.messages.next({
                    text: `There are ${data} users in the chat.`
                });
            });

            this.socket.on('message', (data: object) => {
                this.messages.next(data);
            });
        });
    }

    private listenToEvents() {
        // Make a connection with the Laravel Echo server
        this.echo = new Echo({
            broadcaster: 'socket.io',
            host: environment.echoServerUrl,
            client: io
        });

        this.echo.channel('announcements')
            .listen('Announcement', (data: any) => {
                this.messages.next(data);
            });
    }

    public sendMessage(message: string) {
        if (message.length > 0) {
            this.socket.send(message);
        }
    }
}
