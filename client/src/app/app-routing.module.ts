import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ChatComponent } from './features/chat/chat.component';

const appRoutes: Routes = [
    {
        path: 'chat',
        component: ChatComponent
    },
    {
        path: '**',
        component: ChatComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes
        )
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule { }
