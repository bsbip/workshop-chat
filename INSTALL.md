# Installation guide

This install guide is created for a Windows environment.

## Installing the required programs

1. Install [Git bash](https://gitforwindows.org/)
1. Install [Redis](https://github.com/MicrosoftArchive/redis/releases)
1. Install [NodeJs & npm](https://nodejs.org/en/)
1. Install [PHP 7.2](https://windows.php.net/download/)  
   Extract the zip to: `/c/Program Files/php/7.2`
1. Install [Composer](https://getcomposer.org/doc/00-intro.md)

## Project setup

All commands that are shown below need to be executed in the separate project directories.

### API

1. Make sure you've enabled the `fileinfo` php extension  
   Replace `;extension=fileinfo` with `extension=fileinfo` in the `php.ini` file
1. Run `composer install`
1. Duplicate the `.env.example` file and name it `.env`

### Client

1. Install TypeScript globally, `npm install -g typescript`
1. Install Angular cli, `npm install -g @angular/cli`
1. Run `npm install`

### Laravel echo server

1. Run `npm install`

### Server

1. Install nodemon globally, `npm install -g nodemon`
1. Run `npm install`
1. Duplicate `.env.example` and name it `.env`  
   In the new environment file set the `PORT` constant to `5555`

## Running the projects

- Make sure Redis is running  
  Go to the Redis installation folder and run the `redis-server.exe`. It is normal that the program exits straight away.
- Starting the API  
  `php artisan serve`
- Starting Laravel Echo  
  `npm start`
- Starting the Node server  
  `npm start`
- Starting the client  
  `ng serve`
