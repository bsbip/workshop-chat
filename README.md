# Workshop chat

This repository contains all the files for the chat workshop.

Make sure you have Ansible installed. Installation instructions can be found in the Ansible documentation: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-the-control-machine

To install dependencies for development, run this command: 
`ansible-playbook --connection=local playbooks/dependencies.yml`.